﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OntologyAppDBConnector;
using Ontology_Module;
using OntologyClasses.BaseClasses;

namespace TextFileExport_Module
{
    public class clsDataWork_TextTemplates
    {
        public clsLocalConfig objLocalConfig;

        private OntologyModDBConnector objDBLevel_Ref_To_TextTemplates;
        private OntologyModDBConnector objDBLevel_TextTemplates;
        private OntologyModDBConnector objDBLevel_TextTemplates_To_Variable;

        public clsOntologyItem OItem_Result_Ref_To_TextTemplates { get; private set; }
        public clsOntologyItem OItem_Result_TextTemplates { get; private set; }
        public clsOntologyItem OItem_Result_Variables { get; private set; }
        
        public List<clsOntologyItem> Objects1 { get; private set; }
        public clsOntologyItem OItem_Class_Object { get; private set; }
        public clsOntologyItem OItem_RelationType { get; private set; }

        public List<clsObjectRel> OList_Ref_To_TextTemplates
        {
            get { return objDBLevel_Ref_To_TextTemplates.ObjectRels; }
        }

        public List<clsObjectAtt> OList_TextTemplates
        {
            get { return objDBLevel_TextTemplates.ObjAtts; }
        }

        public List<clsObjectRel> OList_TextTemplates_To_Variables
        {
            get { return objDBLevel_TextTemplates_To_Variable.ObjectRels; }
        }

        public clsOntologyItem GetData(List<clsOntologyItem> Objects1, clsOntologyItem OItem_RelationType)
        {
            var objOItem_Result = objLocalConfig.Globals.LState_Success.Clone();

            this.Objects1 = Objects1;
            this.OItem_RelationType = OItem_RelationType;

            objOItem_Result = GetDataInitialize();

            return objOItem_Result;
        }

        public clsOntologyItem GetData(clsOntologyItem OItem_Class_Object, clsOntologyItem OItem_RelationType)
        {
            this.OItem_Class_Object = OItem_Class_Object;
            this.OItem_RelationType = OItem_RelationType;

            var objOItem_Result = GetDataInitialize();

            return objOItem_Result;
        }

        private clsOntologyItem GetDataInitialize()
        {
            var objOItem_Result = objLocalConfig.Globals.LState_Success.Clone();
            GetData_001_Ref_To_TextTemplates();
            objOItem_Result = OItem_Result_Ref_To_TextTemplates;
            if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
            {
                GetData_002_TextTemplates();
                objOItem_Result = OItem_Result_TextTemplates;
                if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                {
                    GetData_003_Variables();
                    objOItem_Result = OItem_Result_Variables;
                }
            }
            

            return objOItem_Result;
        }

        public void GetData_001_Ref_To_TextTemplates()
        {
            var objOItem_Result = objLocalConfig.Globals.LState_Success.Clone();

            List<clsObjectRel> objORelS_Ref_To_TextTemplates;

            if (Objects1 != null && OItem_RelationType != null)
            {
                objORelS_Ref_To_TextTemplates = Objects1.Select(o => new clsObjectRel
                {
                    ID_Object = o.GUID,
                    ID_RelationType = OItem_RelationType.GUID,
                    ID_Parent_Other = objLocalConfig.OItem_class_text_templates.GUID
                }).ToList();
            }
            else
            {
                objORelS_Ref_To_TextTemplates = new List<clsObjectRel> 
                {
                    new clsObjectRel 
                    {
                        ID_Parent_Object = OItem_Class_Object.GUID,
                        ID_RelationType = OItem_RelationType.GUID,
                        ID_Parent_Other = objLocalConfig.OItem_class_text_templates.GUID
                    }
                };
            }

            objOItem_Result = objDBLevel_Ref_To_TextTemplates.GetDataObjectRel(objORelS_Ref_To_TextTemplates, doIds: false);

            OItem_Result_Ref_To_TextTemplates = objOItem_Result;
        }

        public void GetData_002_TextTemplates()
        {
            var objOItem_Result = objLocalConfig.Globals.LState_Success.Clone();

            List<clsObjectAtt> objORelS_TextTemplates__TemplateText;

            if (objDBLevel_Ref_To_TextTemplates.ObjectRels.Any())
            {
                objORelS_TextTemplates__TemplateText = objDBLevel_Ref_To_TextTemplates.ObjectRels.Select(tt => new clsObjectAtt
                {
                    ID_AttributeType = objLocalConfig.OItem_attributetype_templatetext.GUID,
                    ID_Object = tt.ID_Other
                }).ToList();

                objOItem_Result = objDBLevel_TextTemplates.GetDataObjectAtt(objORelS_TextTemplates__TemplateText, doIds: false);

            }
            else
            {
                objDBLevel_TextTemplates.ObjAtts.Clear();
            }

            


            OItem_Result_TextTemplates = objOItem_Result;
        }

        public void GetData_003_Variables()
        {
            var objOItem_Result = objLocalConfig.Globals.LState_Success.Clone();
            if (objDBLevel_TextTemplates.ObjAtts.Any())
            {
                var objORelS_TextTemplates_To_Variable = objDBLevel_TextTemplates.ObjAtts.Select(tt => new clsObjectRel
                {
                    ID_Object = tt.ID_Object,
                    ID_RelationType = objLocalConfig.OItem_relationtype_contains.GUID,
                    ID_Parent_Other = objLocalConfig.OItem_class_variable.GUID
                }).ToList();

                objOItem_Result = objDBLevel_TextTemplates_To_Variable.GetDataObjectRel(objORelS_TextTemplates_To_Variable, doIds: false);
            }
            else
            {
                objDBLevel_TextTemplates_To_Variable.ObjectRels.Clear();
            }

            OItem_Result_Variables = objOItem_Result;
        }

        public clsDataWork_TextTemplates(clsLocalConfig LocalConfig)
        {
            objLocalConfig = LocalConfig;

            Initialize();
        }

        private void Initialize()
        {
            objDBLevel_Ref_To_TextTemplates = new OntologyModDBConnector(objLocalConfig.Globals);
            objDBLevel_TextTemplates = new OntologyModDBConnector(objLocalConfig.Globals);
            objDBLevel_TextTemplates_To_Variable = new OntologyModDBConnector(objLocalConfig.Globals);
        }
    }
}
